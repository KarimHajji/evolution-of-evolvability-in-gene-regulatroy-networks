extern crate betweterlib;
extern crate rand;

use betweterlib::data::*;
use rand::{Rng, SeedableRng, StdRng};

pub use betweterlib::config::Config;

#[test]
fn test_printing_and_parsing() {
    let seed = [
        1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
        3, 4,
    ];
    let mut rng: StdRng = SeedableRng::from_seed(seed);

    macro_rules! test_write_then_read {
        ($rng:expr, $typename:ident) => {
            let x: $typename = $rng.gen();
            assert_eq!(x, $typename::from(&x.to_string()[..]))
        };
    }

    for _ in 0..100 {
        test_write_then_read!(rng, Gene);
        test_write_then_read!(rng, BSite);
    }
    let c: Chrom = Chrom::random(&mut rng);
    assert_eq!(c, Chrom::from(&c.to_string()[..]));
}

#[test]
fn test_parent_tracing() {
    let cfg = Config::default();
    
    let mut rng = rand::thread_rng();
    let a = Some(Agent::default());
    let offspring = a.reproduce(&mut rng, &cfg, BornStats::default());


    assert!(offspring.is_some());
    if let Some(o) = offspring {
        // assert_eq!(o.generation, 1);
        assert!(o.parent.is_some());

        if let Some(ref _p) = o.parent {
            // assert_eq!(p.generation, 0);
        }
    }

    
}

#[test]
fn test_parent_dying_tracing() {
    let cfg = Config::default();

    let mut rng = rand::thread_rng();
    let mut a = Some(Agent::default());
    let offspring = a.reproduce(&mut rng, &cfg, BornStats::default());

    a.die();
    assert!(offspring.is_some());
    if let Some(o) = offspring {
        // assert_eq!(o.generation, 1);
        assert!(o.parent.is_some());

        if let Some(ref _p) = o.parent {
            // assert_eq!(p.generation, 0);
        }
    }   
}

#[test]
fn test_tracing_a_longer_lineage() {
    let cfg = Config::default();

    let mut offspring = Some(Agent::default());

    for i in 0..10 {
        let mut rng = rand::thread_rng();
        let kid = offspring.reproduce(&mut rng, &cfg, BornStats::default());
        if i == 5 {
            offspring.die();
        }
        offspring = kid;
    }

    let o = offspring.unwrap();
    // println!("{:?}", o.generation);

    for a in o.ancestors() {
        println!("{:?}", a);
    }
}

#[test]
fn test_reproduction() {
    let cfg = Config::default();
  
    let mut rng: StdRng = SeedableRng::from_seed(cfg.agent_seed);
    let agent = Some(Agent::random(&mut rng));
    println!("agent: {}", agent.as_ref().unwrap().chrom);
    let offspring = agent.reproduce(&mut rng, &cfg, BornStats::default());
    println!("offspring: {}", offspring.as_ref().unwrap().chrom);
    let offspring2 = offspring.reproduce(&mut rng, &cfg, BornStats::default());
    println!("offspring2: {}", offspring2.as_ref().unwrap().chrom);
    let offspring3 = offspring2.reproduce(&mut rng, &cfg, BornStats::default());
    println!("offspring3: {}", offspring3.as_ref().unwrap().chrom);
    let offspring4 = offspring3.reproduce(&mut rng, &cfg, BornStats::default());
    println!("offspring4: {}", offspring4.as_ref().unwrap().chrom);
}

#[test]
fn test_path() {
    let mut agent = Agent::default();
    let chrom = Chrom::from("19:1,07:-1,11:-1,18:-1,G04:-1:0,16:1,G09:0:0,06:-1,15:1,02:-1,06:-1,G05:1:0,04:1,19:-1,G11:0:1,13:1,09:-1,G09:-1:1,06:1,00:-1,G12:-1:1,12:1,G04:-1:1,03:1,G09:1:0,18:-1,19:-1,06:1,G05:0:1,17:1,19:-1,17:1,G17:0:0,14:1,G12:1:0,12:1,G12:1:1,12:1,G12:1:1,12:1,G03:1:1,02:-1,13:1,06:-1,G13:1:0,06:1,04:1,G13:1:1,06:1,04:1,G08:1:1,04:-1,19:-1,G12:1:0,00:1,16:1,16:1,G10:2:0,14:1,12:-1,11:-1,G10:1:0,02:-1,G14:0:0,04:-1,G14:-1:0,18:-1,17:-1,15:-1,G02:0:0,14:-1,G00:0:0,09:1,09:-1,16:1,G15:2:0,10:1,14:1,G10:-1:1,07:-1,G01:0:0,08:-1,10:1,09:1,G01:0:1,08:-1,10:1,09:1,G07:-1:1,11:-1,08:-1,09:-1,04:-1,G10:2:0,05:-1,15:-1,G18:0:0,08:-1,G16:-1:0,15:1,00:-1,G16:-1:0,15:1,00:-1,G09:0:0,09:-1,08:-1,16:1,G03:2:0,17:-1,10:-1,02:-1,G01:1:0,04:1,09:-1,G19:0:0,12:1,15:-1,08:1,11:1,01:-1,18:-1,03:1,G00:-1:1,18:-1,17:1,G02:-1:1,00:-1,03:1,G11:1:0,01:1,01:1,05:1,13:-1,17:1,G06:1:1,10:-1,11:1,G19:0:0,01:-1");
    agent.gene_states = chrom.state();
    agent.chrom = chrom;

    for _ in 0..20 {
        println!("{}", &agent.gene_states);
        let oldagent_state = agent.gene_states.clone();
        agent.update();
        if agent.gene_states == oldagent_state { break; }
    }
    
}