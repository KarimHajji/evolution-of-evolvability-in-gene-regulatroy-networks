#![feature(test)]

extern crate test;
extern crate betweterlib;
extern crate rand;

use betweterlib::data::*;
use test::Bencher;
use rand::{SeedableRng, StdRng};


#[bench]
fn grid_150x50_update_10_times(b: &mut Bencher) {
    let seed = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
                1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4];
    let mut rng: StdRng = SeedableRng::from_seed(seed);

    let c = Chrom::from("G18:-1:1,01:1,03:1,G16:2:0,04:1,G11:1:1,11:-1,08:-1,06:1,19:1,08:1,14:-1,13:-1,G09:2:1,16:-1,14:1,13:1,G12:-1:1,04:-1,G16:-1:1,17:1,G09:0:1,18:1,G04:0:1,05:1,G17:0:1,10:-1,00:1,G14:0:1,02:-1,G08:0:1,09:1,18:1,G17:2:1,05:1,15:1,10:1,15:-1,12:-1,00:1,18:1,03:-1,G10:2:1,05:-1,12:1,G19:-1:1,17:1,G12:0:1,09:1,18:-1,19:1,05:-1,G05:2:1,03:-1,08:-1,19:-1,11:1,G05:2:1,03:-1,08:-1,19:-1,11:1,G03:-1:1,15:-1,13:-1,G10:-1:1,04:-1,01:1,17:1,G02:2:1,07:-1,13:1,G08:1:1,02:1,G19:1:1,10:1,04:1,19:-1,09:1,13:1,01:-1,00:1,G03:1:1,01:-1,06:-1,05:-1,G12:-1:1,04:1,G00:-1:1,12:-1,10:1,G17:1:1,08:1,14:-1,G06:1:1,05:-1,12:-1,G11:-1:1,12:-1,01:1,13:1,08:-1,G13:0:1,18:-1,G07:-1:1,18:1,09:-1,G16:2:1,17:-1,G15:2:0,05:1,00:1,09:1,G04:2:1,19:1,14:1,08:-1,G16:1:1,08:-1,G18:-1:1,03:1,G02:1:1,15:1,G02:1:1,00:-1,G17:0:1,13:-1,05:1,11:-1,06:1,16:-1,G01:2:1");
    let mut field = Field::with_state(Some(Agent::from(c)));
    let cfg = Config::default();

    b.iter(|| for _ in 0..10 {
        field.update_synchronous(&mut rng, &cfg, BornStats::default())
    })
}