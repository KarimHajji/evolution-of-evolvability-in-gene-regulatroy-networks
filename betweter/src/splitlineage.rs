//This file is not used

extern crate betweterlib;
extern crate itertools;

use betweterlib::data::*;

use itertools::Itertools;
use std::env;
// use std::fs::{File, create_dir, OpenOptions};

// #[derive(Default, Clone)]
// pub struct Config {
//     on_time: Option<usize>,
//     convert_chrom: Vec<fn(Chrom, BornStats) -> String>,
// }

pub fn main() -> std::io::Result<()> {
    let mut funcs: Vec<(&str, fn(Chrom, BornStats) -> String)> = vec![];

    funcs.push(("env", env));
    funcs.push(("time", time));
    funcs.push(("chrom", chrom));
    funcs.push(("chrom_p", chrom_p));

    Ok(())
}

fn env(_c: Chrom, bs: BornStats) -> String {
    bs.env.to_string()
}
fn time(_c: Chrom, bs: BornStats) -> String {
    bs.time.to_string()
}
fn chrom(c: Chrom, _bs: BornStats) -> String {
    format!("{:?}", c)
}
fn chrom_p(c: Chrom, _bs: BornStats) -> String {
    format!("{}", c)
}
// fn attr_is_point(c: Chrom, bs: BornStats) -> String {
//     let a = Agent::from(c);

// }
