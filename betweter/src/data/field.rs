#![allow(dead_code)]

/*
 * The field on which fieldpoints live
 * it is a 2d grid represented by a linear vector of fieldpoints that can be accessed with 2d accessor functions
 *
 * agents implement the fieldpoint trait, so they can live on the field
 * For something to be a fieldpoint it can
 *  - reproduce
 *  - die
 *  - be checked for viability
 *  - has an empty state
 *  - has a deathrate
 *  - has a fitness
 *  - has bornstats
 *
*/

use rand::Rng;
use std::mem;

use super::{HEIGHT, SELECTIONPRESSURE, WIDTH};

pub use super::super::config::Config;

#[derive(Clone, Default)]
pub struct Field<T>
where
    T: FieldPoint,
{
    pub fieldpoints: Vec<T>,
    pub env: u8,
}

impl<T> Field<T>
where
    T: FieldPoint,
{
    pub fn with_state(state: T) -> Self {
        Field {
            fieldpoints: vec![state; WIDTH * HEIGHT],
            env: 0,
        }
    }
    pub fn get(&self, x: usize, y: usize) -> Option<&T> {
        self.fieldpoints.get(x + y * WIDTH)
    }
    fn set(&mut self, x: usize, y: usize, agent: T) {
        let a = &mut self.fieldpoints[Self::cood_to_index(x, y)];
        mem::replace(a, agent);
    }
    fn cood_to_index(x: usize, y: usize) -> usize {
        if x < WIDTH && y < HEIGHT {
            x + y * WIDTH
        } else {
            panic!(
                "index out of range: x: {}, y: {}, i: {}",
                x,
                y,
                x + y * WIDTH
            )
        }
    }
    fn index_to_cood(i: usize) -> (usize, usize) {
        let x = i % WIDTH;
        let y = i / WIDTH;
        if x < WIDTH && y < HEIGHT {
            (x, y)
        } else {
            panic!("index out of range: x: {}, y: {}, i: {}", x, y, i)
        } // this should never happen
    }
    fn update_cell<R: Rng>(
        neighbours: &[&T],
        cell: &mut T,
        env: u8,
        rng: &mut R,
        cfg: &Config,
        stats: BornStats,
    ) {
        if rng.gen::<f64>() < cell.deathrate() {
            cell.die();
            return;
        }
        if cell.is_alive() {
            return;
        }
        let fitnesses: Vec<_> = neighbours.iter().map(|a| a.fitness(env, cfg)).collect();

        let fitness_empty = 0.4f64.powf(SELECTIONPRESSURE);
        let sum: f64 = fitnesses.iter().sum::<f64>() + fitness_empty;
        let r = rng.gen_range(0f64, sum);

        let mut cum_f = 0f64;
        for (i, f) in fitnesses.iter().enumerate() {
            cum_f += f;
            if cum_f > r {
                *cell = neighbours[i].reproduce(rng, cfg, stats);
                return;
            }
        }
    }

    pub fn update_synchronous<R: Rng>(&mut self, rng: &mut R, cfg: &Config, stats: BornStats) {
        let old_field = self.clone();
        for (i, cell) in self.fieldpoints.iter_mut().enumerate() {
            let (x, y) = Self::index_to_cood(i);
            let neighbours = old_field.moore8(x, y);
            Self::update_cell(&neighbours, cell, self.env, rng, cfg, stats);
        }
    }
    pub fn update_asynchronous<R: Rng>(&mut self, _rng: &mut R) {
        unimplemented!("update self asynchronously")
    }
    fn moore8(&self, i_u: usize, j_u: usize) -> Vec<&T> {
        let i = i_u as isize;
        let j = j_u as isize;
        [
            (i - 1, j - 1),
            (i, j - 1),
            (i + 1, j - 1),
            (i - 1, j),
            (i + 1, j),
            (i - 1, j + 1),
            (i, j + 1),
            (i + 1, j + 1),
        ]
            .iter()
            .filter(|&(x, y)| x.is_positive() && y.is_positive())
            .filter_map(|&(x, y)| self.get(x as usize, y as usize))
            .collect()
    }
}

impl<T> super::Random for Field<T>
where
    T: super::Random + FieldPoint,
{
    fn random<R: rand::Rng>(rng: &mut R) -> Self {
        let a = T::random(rng);
        Field {
            fieldpoints: vec![a; WIDTH * HEIGHT],
            env: 0,
        }
    }
}

pub trait FieldPoint: super::Fitness + Clone {
    fn reproduce<R: rand::Rng>(&self, &mut R, cfg: &Config, stats: BornStats) -> Self;
    fn deathrate(&self) -> f64;
    fn die(&mut self);
    fn empty_state() -> Self;
    fn is_alive(&self) -> bool;
    fn set_born_stats(&mut self, BornStats);
}
impl<T> super::Fitness for Option<T>
where
    T: super::Fitness,
{
    fn fitness(&self, env: u8, cfg: &Config) -> f64 {
        self.as_ref().map_or(0f64, |a| a.fitness(env, cfg))
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Default)]
pub struct BornStats {
    pub env: u8,
    pub time: usize,
}
