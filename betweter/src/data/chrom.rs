/*
 * This file describes a chromosome as a string of Loci.
 *  - its text representation
 *  - its parsing from text
 *  - its updating as a function of the current gene state vector
 *  - its generation using random binding sites and genes (loci)
 *      - it randomly takes 40 genes and 100 binding sites and shuffles the result until every gene has a binding site.
 *  - its fitness
*/
use data::locus::Loc::{BS, GN};
use data::locus::{BSite, Gene, Loc};
use data::Random;
use std::fmt;

use super::super::config::Config;

use rand::distributions::Standard;

use super::{GENETYPES, SELECTIONPRESSURE, TARGET0, TARGET1};

#[derive(Debug, Clone, PartialEq, Default)]
pub struct Chrom(pub Vec<Loc>);
impl fmt::Display for Chrom {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut comma_separated = String::new();
        let list = &self.0;

        for elem in &list[0..list.len() - 1] {
            comma_separated.push_str(&elem.to_string());
            comma_separated.push_str(",");
        }

        comma_separated.push_str(&list[list.len() - 1].to_string());
        write!(f, "{}", comma_separated)
    }
}
impl<'a> From<&'a str> for Chrom {
    fn from(s: &str) -> Self {
        Chrom(s.split(|c| c == ',').map(Loc::from).collect())
    }
}

impl Chrom {
    pub fn state(&self) -> GeneStates {
        let mut gene_states = [0; 20];
        for loc in &self.0 {
            if let GN(ref g) = loc {
                gene_states[g.id as usize] += g.state();
            }
        }
        GeneStates(gene_states)
    }

    pub fn update(&mut self, gene_states: &GeneStates) {
        let mut current_effect = 0;
        for loc in &mut self.0 {
            match *loc {
                GN(ref mut g) => {
                    g.state = current_effect > g.thres;
                    current_effect = 0;
                }
                BS(ref b) => current_effect += gene_states.0[b.id as usize] as i8 * b.weight,
            }
        }
    }
    pub fn has_all_genes(&self) -> bool {
        let mut gene_count = [false; 20];
        for loc in &self.0 {
            if let GN(ref g) = loc {
                gene_count[g.id as usize] = true;
            }
        }
        gene_count.iter().all(|&c| c)
    }
}
impl Random for Chrom {
    fn random<R: rand::Rng>(rng: &mut R) -> Self {
        // A chrom is correct when every gene has at least one binding site.
        // In other words if there are no consecutive genes.
        // A gene in front of the chromosome can therefore still not have a binding site.
        let generated_chrom_is_correct = |chrom: &[_]| {
            let is_gene = |loc: &Loc| if let GN(_) = *loc { true } else { false };
            !chrom.windows(2).any(|both| both.iter().all(is_gene))
        };

        let mut chrom = vec![];
        chrom.extend(
            rng.sample_iter(&Standard)
                .take(GENETYPES)
                .zip(0..GENETYPES as u8)
                .map(|(g, id)| GN(Gene { id, ..g })),
        );
        chrom.extend(
            rng.sample_iter(&Standard)
                .take(GENETYPES) //* 40 genes total
                .map(GN),
        );
        chrom.extend(
            rng.sample_iter(&Standard)
                .take(GENETYPES)
                .zip(0..GENETYPES as u8)
                .map(|(bs, id)| BS(BSite { id, ..bs })),
        );
        chrom.extend(
            rng.sample_iter(&Standard)
                .take(80) //* 100 binding sites total
                .map(BS),
        );

        while !generated_chrom_is_correct(&chrom) {
            rng.shuffle(chrom.as_mut_slice());
        }
        Chrom(chrom)
    }
}

#[derive(Debug, PartialEq, Clone, Eq, Hash, Default)]
pub struct GeneStates(pub [u8; GENETYPES as usize]);

impl fmt::Display for GeneStates {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl GeneStates {
    pub fn hammdist(&self, env: u8) -> usize {
        fn on_off_diff(x: &(&u8, &u8)) -> bool {
            match x {
                (0, 0) => false,
                (0, _) | (_, 0) => true,
                _ => false,
            }
        }
        let target = match env {
            0 => &TARGET0.0,
            1 => &TARGET1.0,
            _ => panic!("Environment must be 0 or 1, but it is {}", env),
        };

        self.0.iter().zip(target.iter()).filter(on_off_diff).count()
    }
}

impl super::Fitness for GeneStates {
    fn fitness(&self, env: u8, _cfg: &Config) -> f64 {
        let d = self.hammdist(env) as f64;
        (1. - d / GENETYPES as f64).powf(SELECTIONPRESSURE)
    }
}
