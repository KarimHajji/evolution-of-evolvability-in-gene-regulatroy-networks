/*
 * This file describes a Lucus (Loc) as either a binding site or a gene.
 *  - their text representation
 *  - their parsing from text
 *  - the generation of a random binding site or gene
*/

use std::fmt;

use super::{GENETYPES, MAXTHRES, MINTHRES};
use rand::distributions::{Distribution, Standard};

pub use self::Loc::{BS, GN};
#[derive(Debug, Clone, PartialEq)]
pub enum Loc {
    GN(Gene),
    BS(BSite),
}
impl Loc {
    pub fn is_gene(&self) -> bool {
        if let GN(_) = self {
            true
        } else {
            false
        }
    }
    pub fn is_bsite(&self) -> bool {
        if let BS(_) = self {
            true
        } else {
            false
        }
    }
    pub fn id(&self) -> u8 {
        match self {
            GN(g) => g.id,
            BS(b) => b.id,
        }
    }
}
impl fmt::Display for Loc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            GN(g) => g.fmt(f),
            BS(b) => b.fmt(f),
        }
    }
}
impl<'a> From<&'a str> for Loc {
    fn from(s: &str) -> Self {
        match s.get(0..1) {
            Some("G") => GN(Gene::from(s)),
            Some(_) => BS(BSite::from(s)),
            None => panic!("Error parsing locus: supplied string empty"),
        }
    }
}
#[derive(Debug, Clone, PartialEq)]
pub struct Gene {
    pub id: u8,
    pub thres: i8,
    pub state: bool,
}
impl fmt::Display for Gene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "G{:02}:{}:{}", self.id, self.thres, self.state())
    }
}
impl<'a> From<&'a str> for Gene {
    fn from(s: &str) -> Self {
        let s = &s[1..];
        let v: Vec<_> = s.split(|c| c == ':').collect();
        let id = v
            .get(0)
            .expect("Error parsing gene: empty string")
            .parse()
            .expect("Error parsing gene: id not integral");
        let thres = v
            .get(1)
            .expect("Error parsing gene: no thres")
            .parse()
            .expect("Error parsing gene: thres not integral");
        let state_i: u8 = v
            .get(2)
            .expect("Error parsing gene: no state")
            .parse()
            .expect("Error parsing gene: state not integral");
        let state: bool = match state_i {
            0 => false,
            1 => true,
            _ => panic!("Error parsing binding site: state not 0 or 1"),
        };
        if id >= GENETYPES as u8 {
            panic!("Error parsing binding site: id larger than {}", GENETYPES)
        }
        if thres < MINTHRES || thres > MAXTHRES {
            panic!("Error parsing binding site: thres out of range")
        }
        Gene { id, thres, state }
    }
}
impl Gene {
    pub fn state(&self) -> u8 {
        self.state as u8
    }
}

impl Distribution<Gene> for Standard {
    fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Gene {
        Gene {
            id: rng.gen_range(0, GENETYPES as u8),
            thres: rng.gen_range(MINTHRES, MAXTHRES + 1),
            state: rng.gen(),
        }
    }
}
impl Default for Gene {
    fn default() -> Self {
        Gene {
            id: 0,
            thres: 0,
            state: false,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct BSite {
    pub id: u8,
    pub weight: i8,
}
impl BSite {
    pub fn id(&self) -> u8 {
        self.id
    }
    pub fn weight(&self) -> i8 {
        self.weight
    }
}
impl fmt::Display for BSite {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:02}:{}", self.id, self.weight)
    }
}
impl<'a> From<&'a str> for BSite {
    fn from(s: &str) -> Self {
        let v: Vec<_> = s.split(|c| c == ':').collect();
        let id = v
            .get(0)
            .expect("Error parsing binding site: empty string")
            .parse()
            .expect("Error parsing binding site: id not integral");
        let weight = v
            .get(1)
            .expect("Error parsing binding site: no weight")
            .parse()
            .expect("Error parsing binding site: weight not integral");
        if id >= GENETYPES as u8 {
            panic!("Error parsing binding site: id out of range ({})", id)
        }
        if !(weight == -1 || weight == 1) {
            panic!(
                "Error parsing binding site: weight out of range ({})",
                weight
            )
        }
        BSite { id, weight }
    }
}
impl Distribution<BSite> for Standard {
    fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> BSite {
        BSite {
            id: rng.gen_range(0, GENETYPES as u8),
            weight: if rng.gen() { 1 } else { -1 },
        }
    }
}
impl Default for BSite {
    fn default() -> Self {
        BSite { id: 0, weight: 1 }
    }
}
