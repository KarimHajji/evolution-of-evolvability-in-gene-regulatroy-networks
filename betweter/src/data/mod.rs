/*
 * This file contains the model parameters as well as some definitions of universal traits of the data structures
 */

pub mod agent;
pub mod chrom;
pub mod field;
pub mod locus;
pub mod mutations;

pub use self::agent::*;
pub use self::chrom::*;
pub use self::field::*;
pub use self::locus::*;
pub use self::mutations::*;

pub use rand;
pub use rand::distributions::{Distribution, Standard};

//*----PARAMETERS-------------------------------------------------------------//
pub const GENETYPES: usize = 20;
pub const MINTHRES: i8 = -1; //inclusive
pub const MAXTHRES: i8 = 2; //inclusive

use data::chrom::GeneStates;
pub const TARGET0: GeneStates =
//             \      household       \    specific     \
    GeneStates([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]);
pub const TARGET1: GeneStates =
    GeneStates([1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]);
pub const RESET_STATE: GeneStates =
    GeneStates([1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]);
pub const SELECTIONPRESSURE: f64 = 10.;
// pub const MAX_UPDATE_STEPS: usize = 20; // not used because whole attractor is used to determine fitness
pub const HEIGHT: usize = 50;
pub const WIDTH: usize = 150;

pub const DEATHRATE: f64 = 0.3;

pub const P_GEN_DEL: f64 = 3e-4_f64;
pub const P_GEN_DUP: f64 = 2e-4_f64;
pub const P_GEN_THRES_CH: f64 = 5e-6_f64;
pub const P_TFBS_WT_CH: f64 = 2e-5_f64;
pub const P_TFBS_PREF_CH: f64 = 2e-5_f64;
pub const P_TFBS_DEL: f64 = 3e-5_f64;
pub const P_TFBS_INNOV: f64 = 1e-5_f64;
pub const P_TFBS_DUP: f64 = 2e-5_f64;

pub const OUTPUT_STEP_SIZE: usize = 50;
pub const LINEAGE_STEP_SIZE: usize = 1000;
pub const P_ENV_SWITCH: f64 = 3e-4 as f64;
//*END-PARAMETERS-------------------------------------------------------------//

pub trait Random {
    fn random<R: rand::Rng>(rng: &mut R) -> Self;
}

impl<T> Random for Option<T>
where
    T: Random,
{
    fn random<R: rand::Rng>(rng: &mut R) -> Self {
        Some(T::random(rng))
    }
}

pub trait Fitness {
    fn fitness(&self, env: u8, cfg: &Config) -> f64;
}
