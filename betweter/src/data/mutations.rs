/*
 * This file describes the myriad mutations that can happen.
 * It also describes logging of these mutations
 */

use data::agent::Agent;
use data::chrom::Chrom;
use data::locus::{BSite, Gene, Loc, BS, GN};

pub use super::super::config::Config;

use data::{GENETYPES, MAXTHRES, MINTHRES};
use data::{
    P_GEN_DEL, P_GEN_DUP, P_GEN_THRES_CH, P_TFBS_DEL, P_TFBS_DUP, P_TFBS_INNOV, P_TFBS_PREF_CH,
    P_TFBS_WT_CH,
};

use itertools::Itertools;
use std::fmt;

// pub trait Mutate<R>
// where R: rand::Rng,
//       Self: Sized
// {
//     fn mutate(self, &mut Vec<Mutation>, rng: &mut R) -> Vec<Self>;
// }

#[derive(Clone, PartialEq, Debug)]
pub enum Mutation {
    GenDel(u8),
    GenDup(u8),
    GenThresCh(u8),
    TfbsWtCh(u8),
    TfbsPrefCh(u8),
    TfbsDel(u8),
    TfbsInnov(u8),
    TfbsDup(u8),
}
impl fmt::Display for Mutation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Mutation::GenDel(id) => write!(f, "GenDel (ID {})", id),
            Mutation::GenDup(id) => write!(f, "GenDup (ID {})", id),
            Mutation::GenThresCh(id) => write!(f, "GenThresCh (ID {})", id),
            Mutation::TfbsWtCh(id) => write!(f, "TfbsWtCh (ID {})", id),
            Mutation::TfbsPrefCh(id) => write!(f, "TfbsPrefCh (ID {})", id),
            Mutation::TfbsDel(id) => write!(f, "TfbsDel (ID {})", id),
            Mutation::TfbsInnov(id) => write!(f, "TfbsInnov (ID {})", id),
            Mutation::TfbsDup(id) => write!(f, "TfbsDup (ID {})", id),
        }
    }
}

#[derive(Clone, PartialEq, Debug, Default)]
pub struct Mutations(pub Vec<Mutation>);
impl fmt::Display for Mutations {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut comma_separated = "[".to_string();
        let list = &self.0;

        if list.is_empty() {
            return write!(f, "[]");
        }

        for elem in &list[0..list.len() - 1] {
            comma_separated.push_str(&elem.to_string());
            comma_separated.push_str(",");
        }

        comma_separated.push_str(&list[list.len() - 1].to_string());
        comma_separated.push_str("]");

        write!(f, "{}", comma_separated)
    }
}
impl Mutations {
    fn add(&mut self, m: Mutation) {
        self.0.push(m);
    }
}

impl Gene {
    fn mutate<R: rand::Rng>(self, muts: &mut Vec<Mutation>, rng: &mut R) -> Vec<Self> {
        let id = self.id;
        let mut result = vec![self];
        if P_GEN_THRES_CH > rng.gen() {
            muts.push(Mutation::GenThresCh(id));
            result[0].thres = rng.gen_range(MINTHRES, MAXTHRES + 1);
        }
        result
    }
}

impl BSite {
    fn mutate<R: rand::Rng>(self, muts: &mut Vec<Mutation>, rng: &mut R) -> Vec<Self> {
        let id = self.id;
        let mut result = vec![self];
        if P_TFBS_WT_CH > rng.gen() {
            muts.push(Mutation::TfbsWtCh(id));
            result[0].weight = if rng.gen() { 1 } else { -1 };
        }
        if P_TFBS_PREF_CH > rng.gen() {
            muts.push(Mutation::TfbsPrefCh(id));
            result[0].id = rng.gen_range(0, GENETYPES as u8);
        }
        if P_TFBS_DUP > rng.gen() {
            muts.push(Mutation::TfbsDup(id));
            let t = result[0].clone();
            result.push(t);
        }
        if P_TFBS_DEL > rng.gen() {
            muts.push(Mutation::TfbsDel(id));
            result.pop();
        }
        result
    }
}

impl Loc {
    fn mutate<R: rand::Rng>(self, muts: &mut Vec<Mutation>, rng: &mut R) -> Vec<Self> {
        match self {
            GN(g) => {
                let mut v: Vec<Loc> = g.mutate(muts, rng).into_iter().map(GN).collect();
                if P_TFBS_INNOV > rng.gen() {
                    v.push(BS(rng.gen()));
                    muts.push(Mutation::TfbsInnov(v[1].id()));
                }
                v
            }
            BS(b) => b.mutate(muts, rng).into_iter().map(BS).collect(),
        }
    }
}

impl Chrom {
    fn mutate<R: rand::Rng>(self, muts: &mut Vec<Mutation>, rng: &mut R) -> Self {
        if self.0.is_empty() {
            println!("mutating empty chrom");
        }

        let mut mutated = vec![];
        let mut list_of_duplicated_locs = vec![];

        //First mutate all locs individually
        //If they duplicate keep track of that in list_of_duplicated_locs
        //and insert those after
        for loc in self.0.into_iter() {
            let mut loc_m = loc.mutate(muts, rng).into_iter();

            if let Some(l) = loc_m.next() {
                mutated.push(l);
            }

            for l in loc_m {
                list_of_duplicated_locs.push(l);
            }
        }

        for loc in list_of_duplicated_locs.into_iter() {
            let pos = rng.gen_range(0, mutated.len());
            mutated.insert(pos, loc);
        }
        // let mutated: Vec<Loc> =
        //     self.0.into_iter()
        //     .flat_map(|l| l.mutate(muts, rng)).collect();

        // Now Gene+bindingsite duplication/deletion

        let mut pos_of_genes: Vec<usize> = vec![0];
        for (i, l) in mutated.iter().enumerate() {
            if let GN(_) = l {
                if i != 0 {
                    pos_of_genes.push(i);
                }
            }
        }

        if mutated.len() < 2 {
            return Chrom(mutated);
        }

        pos_of_genes.push(mutated.len());
        // pos_of_genes.push(mutated.len());

        let mut result: Vec<Loc> = vec![];

        for (i, j) in pos_of_genes.into_iter().tuple_windows() {
            if let Some(gene) = mutated.get(i..j) {
                assert!(gene[..].last().unwrap().is_gene());z

                let id = gene[0].id();
                if P_GEN_DUP > rng.gen() {
                    muts.push(Mutation::GenDup(id));
                    result.extend_from_slice(gene);
                }
                if P_GEN_DEL > rng.gen() {
                    muts.push(Mutation::GenDel(id));
                    continue;
                }
                result.extend_from_slice(gene);
            }
        }

        Chrom(result)
    }
}

impl Agent {
    pub fn mutate<R: rand::Rng>(&mut self, muts: &mut Vec<Mutation>, rng: &mut R, _cfg: &Config) {
        let c = self.chrom.clone();
        // let old_c = c.clone();

        self.chrom = c.mutate(muts, rng);
    }
}

// impl Agent
// {
//     pub fn mutate<R: rand::Rng>(&mut self, muts: &mut Vec<Mutation>, rng: &mut R) {
//         let c = self.chrom.clone();
//         self.chrom = c.mutate(muts, rng);
//     }
// }
