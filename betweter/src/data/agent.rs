/*
 * This file describes an agent as a chromosome (chrom),
 * a current gene state vector (gene_states),
 * a reference to a parent it came from (parent).
 * the mutation that made it different from its parent (muts),
 * the enviroment present when it was born,
 * functions:
 *  - its text representation
 *  - its parsing from text
 *  - its updating
 *  - its updating until it is in an attractor
 *  - its random generation
 *      - a random chromosome, udpated until its in an attractor state
 *  - the generation of offspring (reproduce)
 *  - the generation of a lineage trace from an agent
 *      - iterating through the parents
*/
use data::chrom::*;
use data::mutations::Mutations;
use data::Fitness;
use data::Random;
use data::DEATHRATE;
use data::RESET_STATE;

use data::field::BornStats;

pub use super::super::config::Config;

use std::rc::Rc;
// use std::boxed::Box;
use std::collections::HashSet;
use std::fmt;

#[derive(Debug, Clone, Default)]
pub struct Agent {
    pub chrom: Chrom,
    pub gene_states: GeneStates, //Represents current state of chrom
    pub parent: Option<Rc<Agent>>,
    pub muts: Mutations,
    pub born_stats: BornStats,
}

impl fmt::Display for Agent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{};{};{};{}",
            self.born_stats.time, self.born_stats.env, self.chrom, self.muts,
        )
    }
}

impl From<Chrom> for Agent {
    fn from(chrom: Chrom) -> Self {
        let mut a = Agent::default();
        a.gene_states = chrom.state();
        a.chrom = chrom;
        a.find_attractor();
        a
    }
}

impl Agent {
    pub fn update(&mut self) {
        self.chrom.update(&self.gene_states);
        self.gene_states = self.chrom.state();
    }

    pub fn find_attractor(&mut self) {
        let mut set = HashSet::new();

        while set.insert(self.gene_states.clone()) {
            self.update();
        }
    }
}
impl Fitness for Agent {
    //this only works if the agent is already in an attractor state.
    fn fitness(&self, env: u8, cfg: &Config) -> f64 {
        // if let Some(f) = self.fitness {
        //     f
        // } else {
        // let mut a = self.clone();

        // let v = a.find_attractor();
        // let l = v.len() as f64;

        // v.iter().map(|gs| gs.fitness(env, cfg)).sum::<f64>() / l
        // }
        // self.gene_states.fitness(env)

        let mut a = self.clone();
        let fst_gs = a.gene_states.clone();

        a.update();
        let mut sum = fst_gs.fitness(env, cfg);
        let mut count = 1f64;
        while a.gene_states != fst_gs {
            sum += a.gene_states.fitness(env, cfg);
            count += 1f64;
            a.update();
        }
        sum / count
    }
}
impl Random for Agent {
    fn random<R: rand::Rng>(rng: &mut R) -> Self {
        let mut a = Agent::default();
        a.chrom = Chrom::random(rng);
        a.gene_states = a.chrom.state();
        a.find_attractor();
        a
    }
}

impl super::field::FieldPoint for Option<Agent> {
    fn reproduce<R: rand::Rng>(&self, rng: &mut R, cfg: &Config, stats: BornStats) -> Self {
        if let Some(p) = self {
            /*
             * clone the parent
             * set bornstats property
             * mutate the clone
             * if it has mutated:
             *  check if it is viable (has all genes at least once)
             *  set mutation property
             *  if reset genestates -> reset genestates
             *  get into attractor
             *  set parent property
             * return
             */
            let mut new = p.clone();
            new.born_stats = stats;

            let mut muts = vec![];
            new.mutate(&mut muts, rng, cfg);
            if !muts.is_empty() {
                if !new.chrom.has_all_genes() {
                    return None;
                }

                new.muts = Mutations(muts);

                if cfg.reset_genestates_on_birth {
                    new.gene_states = RESET_STATE;
                }

                new.find_attractor();

                // new.fitness = None;
                // let f = new.fitness(stats.env, &cfg);
                // new.fitness = Some(f);

                new.parent = Some(Rc::new(p.clone()));
            }
            Some(new)
        } else {
            None
        }
    }
    fn deathrate(&self) -> f64 {
        DEATHRATE
    }
    fn die(&mut self) {
        self.take();
    }
    fn empty_state() -> Self {
        None
    }
    fn is_alive(&self) -> bool {
        self.is_some()
    }
    fn set_born_stats(&mut self, stats: BornStats) {
        if let Some(a) = self {
            a.born_stats = stats;
        }
    }
}

pub struct Lineage<'a> {
    agent: Option<&'a Agent>,
}

impl Agent {
    pub fn ancestors(&self) -> Lineage {
        Lineage { agent: Some(self) }
    }
}

impl<'a> Iterator for Lineage<'a> {
    type Item = &'a Agent;

    fn next(&mut self) -> Option<Self::Item> {
        let result = self.agent;
        if let Some(a) = self.agent {
            self.agent = a.parent.as_ref().map(|p| &**p);
        }
        result
    }
}
