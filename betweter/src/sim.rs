
/*
 * In this file the main function is defined.
 * It initializes the agent and the field and defines the main loop.
 */

extern crate betweterlib;
extern crate sdl2;
extern crate rand;

use betweterlib::data::field::*;
use betweterlib::data::Fitness;
use betweterlib::data::{OUTPUT_STEP_SIZE, LINEAGE_STEP_SIZE, P_ENV_SWITCH};
use betweterlib::config;

use betweterlib::graphics;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

// use rand::prng::XorShiftRng;
use rand::{SeedableRng, StdRng, Rng};
use betweterlib::data::{Agent, Random};


use std::io::{SeekFrom, Seek};
use std::io::Write;


pub fn main() -> std::io::Result<()> {
    let mut cfg = config::init()?;

    let mut agent_rng: StdRng = SeedableRng::from_seed(cfg.agent_seed);
    let mut field_rng: StdRng = SeedableRng::from_seed(cfg.field_seed);
    let mut env_rng  : StdRng = SeedableRng::from_seed(cfg.env_seed  );

    let mut field: Field<Option<Agent>> = Field::random(&mut agent_rng);
    field.env = cfg.starting_env;

    if let Some(file) = &mut cfg.output_file {
        writeln!(file, 
            "world-seed={}; agent-seed={}; env-seed={}; reset={}; initialAgent={}",
            cfg.field_seed[0], cfg.agent_seed[0], cfg.env_seed[0], cfg.reset_genestates_on_birth, 
            field.fieldpoints[0].as_ref().unwrap().chrom
        )?;
        writeln!(file, 
            "time;env;minHammDist;minOtherHammDist;avgHammDist;lenBestChrom;bestChrom;bestOtherChrom;geneStateTable")?;
    }
    // uncomment to enable a graphical output of the field
    let (mut r, mut e) = graphics::init();

    //*----THE MAIN LOOP-------------------------------------------------------//
    'running: 
    for t in 0..=10_000_000 {        
        // uncomment to enable a graphical output of the field
        for event in e.poll_iter() {
            if let Event::KeyDown { keycode: Some(Keycode::Escape), .. } = event {
                break 'running;
            }
        }
        graphics::display_field(&mut r, &field, &cfg);

        if t % OUTPUT_STEP_SIZE == 0 {
            // let n = field.fieldpoints.iter().filter(|o| o.is_alive()).count();

            let fittest: &Agent = 
                field.fieldpoints.iter()
                    .max_by(|a,b| a.fitness(field.env, &cfg)
                                .partial_cmp(
                                    &b.fitness(field.env, &cfg)
                                    ).unwrap()
                    ).unwrap().as_ref().unwrap();
            let other_env = (field.env + 1) % 2;
            let other_fittest: &Agent = 
                field.fieldpoints.iter()
                    .max_by(|a,b| a.fitness(other_env, &cfg)
                                .partial_cmp(
                                    &b.fitness(other_env, &cfg)
                                    ).unwrap()
                    ).unwrap().as_ref().unwrap();
            let hammdists: Vec<_> = field.fieldpoints.iter()
                .filter_map(|a| a.as_ref()) // filter out dead fieldpoints
                .map(|a| a.gene_states.hammdist(field.env))
                .collect();
            let avg_hammdist = 
                hammdists.iter().sum::<usize>() as f64 / hammdists.len() as f64;


            // println!("{}, alive: {}/{}, fitness: {}", t, n, field.fieldpoints.len(), fittest.fitness(field.env));
            // println!("{}", fittest);

            if cfg.console_output {
                println!("{};{};{};{}",
                    t, field.env, fittest.gene_states.hammdist(field.env), fittest.gene_states
                );
            }

            if cfg.env_switching && 
               P_ENV_SWITCH * OUTPUT_STEP_SIZE as f64 > env_rng.gen() {
                match field.env {
                    0 => field.env = 1,
                    _ => field.env = 0,
                }
                // TODO: make this work
                // field.fieldpoints.iter_mut().map(|f| f.map(|a| a.fitness = None));  
            }
            if let Some(file) = &mut cfg.output_file {
                writeln!(file, 
                    "{};{};{};{};{};{};{};{};{}",
                    t, field.env, fittest.gene_states.hammdist(field.env), other_fittest.gene_states.hammdist(other_env), avg_hammdist, fittest.chrom.0.len(), fittest.chrom, other_fittest.chrom, fittest.gene_states
                )?;
                // "time;env;minHammDist;minOtherHammDist;avgHammDist;lenBestChrom;bestChrom;bestOtherChrom;geneStateTable"
            }
        }

        if t % LINEAGE_STEP_SIZE == 0 {
            let fittest_agent = 
                field.fieldpoints
                .iter()
                .max_by(|a, b| 
                    a.fitness(field.env, &cfg).partial_cmp(&b.fitness(field.env, &cfg))
                    .unwrap()
                ).unwrap().as_ref().unwrap();
            
            if let Some(file) = &mut cfg.lineage_file {
                file.seek(SeekFrom::Start(0))?;
                for a in fittest_agent
                            .ancestors().collect::<Vec<_>>()
                            .iter().rev()                    
                {
                    writeln!(file, "{}", a)?;
                }
            }
        }

        let stats = BornStats{ env: field.env, time: t };
        field.update_synchronous(&mut field_rng, &cfg, stats);
    }
    Ok(())
}

