extern crate arrayvec;
extern crate ndarray;
extern crate sdl2;
pub extern crate rand;
extern crate itertools;

pub mod data;
pub mod graphics;
pub mod config;