/*
 * This file describes the displaying of fields according to the fitnesses of fieldpoints
 */

use super::data::{Field, FieldPoint, Fitness, HEIGHT, WIDTH};

use sdl2;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::{CanvasBuilder, WindowCanvas};
use sdl2::EventPump;

use super::config::Config;

const SCALE: usize = 10;
const MAX_X: usize = SCALE * WIDTH - 1;
const MAX_Y: usize = SCALE * HEIGHT - 1;

pub fn init() -> (WindowCanvas, EventPump) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("demo", MAX_X as u32 + 1, MAX_Y as u32 + 1)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = CanvasBuilder::from(window).build().unwrap();

    let event_pump = sdl_context.event_pump().unwrap();

    renderer.set_draw_color(Color::RGB(255, 255, 255));
    renderer.clear();
    renderer.present();

    (renderer, event_pump)
}

fn display_cell(r: &mut WindowCanvas, row: usize, col: usize, fitness: f64) {
    let x = SCALE * col;
    let y = SCALE * row;

    let cell_color = Color::RGB((255.0 * fitness) as u8, 0, 0);

    r.set_draw_color(cell_color);
    r.fill_rect(Rect::new(x as i32, y as i32, SCALE as u32, SCALE as u32))
        .unwrap_or_else(|err| panic!("Error drawing rectangle:\n{:?}", err));
}

pub fn display_field<T>(r: &mut WindowCanvas, field: &Field<T>, cfg: &Config)
where
    T: FieldPoint + Fitness,
{
    r.set_draw_color(Color::RGB(200, 200, 200));
    r.clear();
    for row in 0..HEIGHT {
        for col in 0..WIDTH {
            if let Some(a) = field.get(col, row) {
                display_cell(r, row, col, a.fitness(field.env, cfg));
            }
        }
    }
    r.present();
}
