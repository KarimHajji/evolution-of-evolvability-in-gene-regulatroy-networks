// This file is not used

extern crate ndarray;
extern crate rand;
extern crate sdl2;

use self::sdl2::event::Event;
use self::sdl2::keyboard::Keycode;
use self::sdl2::pixels::Color;
use self::sdl2::rect::Rect;
use self::sdl2::render::{CanvasBuilder, WindowCanvas};
use self::sdl2::EventPump;

// use self::ndarray::Vec;

use rand::Rng;

const MAX_X: usize = 1999;
const MAX_Y: usize = MAX_X;
const CELL_WIDTH: usize = 5;
const CELL_HEIGHT: usize = CELL_WIDTH;
const NCELLS: usize = ((MAX_X + 1) / CELL_WIDTH) as usize;

fn life_random() -> Vec<bool> {
    let mut rng = rand::thread_rng();

    let mut v: Vec<bool> = [false; NCELLS * NCELLS].to_vec();

    for elem in v.iter_mut() {
        *elem = rng.gen();
    }
    v
}

fn display_cell(r: &mut WindowCanvas, row: usize, col: usize) {
    let x = CELL_WIDTH * col;
    let y = CELL_WIDTH * row;

    let cell_color = Color::RGB(255, 0, 0);
    r.set_draw_color(cell_color);
    r.fill_rect(Rect::new(
        x as i32,
        y as i32,
        CELL_WIDTH as u32,
        CELL_HEIGHT as u32,
    ))
    .unwrap_or_else(|err| panic!("Error drawing rectangle:\n{:?}", err));
}

fn display_frame(r: &mut WindowCanvas, v: &[bool]) {
    r.set_draw_color(Color::RGB(200, 200, 200));
    r.clear();
    for i in 0..NCELLS {
        for j in 0..NCELLS {
            if v[i + j * NCELLS] {
                display_cell(r, i, j);
            }
        }
    }
    r.present();
}

fn inc(n: usize) -> usize {
    (n + 1) % (NCELLS as usize)
}

fn dec(n: usize) -> usize {
    if n == 0 {
        (NCELLS - 1) as usize
    } else {
        (n - 1) as usize
    }
}

fn count_surrounding(r: usize, c: usize, v: &[bool]) -> usize {
    v[dec(r) + c * NCELLS] as usize
        + v[inc(r) + c * NCELLS] as usize
        + v[r + dec(c) * NCELLS] as usize
        + v[r + inc(c) * NCELLS] as usize
        + v[dec(r) + dec(c) * NCELLS] as usize
        + v[dec(r) + inc(c) * NCELLS] as usize
        + v[inc(r) + inc(c) * NCELLS] as usize
        + v[inc(r) + dec(c) * NCELLS] as usize
}

fn alive(r: usize, c: usize, v: &[bool]) -> bool {
    let n = count_surrounding(r, c, v);
    let curr = v[r + c * NCELLS] as i32;

    match (curr, n) {
        (1, 0...1) => false,
        (1, 4...8) => false,
        (1, 2...3) => true,
        (0, 3) => true,
        (0, 0...2) => false,
        (0, 4...8) => false,
        _ => panic!("alive: error in match"),
    }
}

fn life_next(v: &[bool]) -> Vec<bool> {
    let mut v2: Vec<bool> = [false; NCELLS * NCELLS].to_vec();

    for i in 0..NCELLS {
        for j in 0..NCELLS {
            v2[i + j * NCELLS] = alive(i, j, &v);
        }
    }
    v2
}

fn init() -> (WindowCanvas, EventPump) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("demo", MAX_X as u32 + 1, MAX_Y as u32 + 1)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = CanvasBuilder::from(window).build().unwrap();

    let event_pump = sdl_context.event_pump().unwrap();

    renderer.set_draw_color(Color::RGB(255, 255, 255));
    renderer.clear();
    renderer.present();

    (renderer, event_pump)
}

pub fn main() {
    let (mut r, mut e) = init();
    let mut v = life_random();

    'running: for _ in 0..40_000 {
        for event in e.poll_iter() {
            if let Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } = event
            {
                break 'running;
            }
        }
        v = life_next(&v);
        display_frame(&mut r, &v);
    }
    // std::thread::sleep(std::time::Duration::from_millis(600000));
}
