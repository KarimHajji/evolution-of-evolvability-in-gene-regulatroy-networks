// This file is not used
extern crate ndarray;
extern crate rand;
extern crate sdl2;

use self::sdl2::event::Event;
use self::sdl2::keyboard::Keycode;
use self::sdl2::pixels::Color;
use self::sdl2::rect::Rect;
use self::sdl2::render::{CanvasBuilder, WindowCanvas};
use self::sdl2::EventPump;

use self::ndarray::Array2;

use rand::Rng;

const CELL_WIDTH: usize = 3;
const CELL_HEIGHT: usize = CELL_WIDTH;
const NCELLS: usize = 400 as usize;
const MAX_X: usize = NCELLS * CELL_WIDTH - 1;
const MAX_Y: usize = MAX_X;

fn life_random() -> Array2<bool> {
    let mut rng = rand::thread_rng();

    let mut v: Array2<bool> = Array2::from_elem((NCELLS, NCELLS), false);

    // for elem in v.iter_mut(){
    //     *elem = rng.gen();
    // }

    for i in 0..50 {
        for j in 0..50 {
            _glider(&mut v, i * 7, j * 7);
        }
    }

    v
}

fn _glider(v: &mut Array2<bool>, r: usize, c: usize) {
    v[[10 + r, 11 + c]] = true;
    v[[11 + r, 12 + c]] = true;
    v[[12 + r, 10 + c]] = true;
    v[[12 + r, 11 + c]] = true;
    v[[12 + r, 12 + c]] = true;
}

fn display_cell(r: &mut WindowCanvas, row: usize, col: usize) {
    let x = CELL_WIDTH * col;
    let y = CELL_WIDTH * row;

    let cell_color = Color::RGB(255, 0, 0);
    r.set_draw_color(cell_color);
    r.fill_rect(Rect::new(
        x as i32,
        y as i32,
        CELL_WIDTH as u32,
        CELL_HEIGHT as u32,
    ))
    .unwrap_or_else(|err| panic!("Error drawing rectangle:\n{:?}", err));
}

fn display_frame(r: &mut WindowCanvas, v: &Array2<bool>) {
    r.set_draw_color(Color::RGB(200, 200, 200));
    r.clear();
    for i in 0..NCELLS {
        for j in 0..NCELLS {
            if v[[i, j]] {
                display_cell(r, i, j);
            }
        }
    }
    r.present();
}

fn inc(n: usize) -> usize {
    match n + 1 {
        NCELLS => 0,
        m => m,
    }
}

fn dec(n: usize) -> usize {
    match n {
        0 => NCELLS - 1,
        _ => n - 1,
    }
}

fn moore8<T>(v: &Array2<T>, r: usize, c: usize) -> [&T; 8] {
    // let i = i_u as isize; let j = j_u as isize;
    [
        &v[[dec(r), c]],
        &v[[inc(r), c]],
        &v[[r, dec(c)]],
        &v[[r, inc(c)]],
        &v[[dec(r), dec(c)]],
        &v[[dec(r), inc(c)]],
        &v[[inc(r), inc(c)]],
        &v[[inc(r), dec(c)]],
    ]
}

fn alive(r: usize, c: usize, v: &Array2<bool>) -> bool {
    // let n = count_surrounding(r, c, v);
    let n = moore8(v, r, c)
        .into_iter()
        .map(|&&b| if b { 1 } else { 0 })
        .sum();

    // if v[[r,c]] {
    //     match n {
    //         3...4 | 6...8 => true,
    //         _ => false
    //     }
    // } else {
    //     match n {
    //         3 | 6...8 => true,
    //         _ => false
    //     }
    // }

    match (v[[r, c]], n) {
        (true, 2...3) => true,
        (false, 3) => true,
        _ => false,
    }
}

fn life_next(v: &mut Array2<bool>) {
    // let mut v2: Array2<bool> = Array2::from_elem((NCELLS, NCELLS), false);
    let mut v2: Array2<bool> = Array2::from_elem((NCELLS, NCELLS), false);

    for i in 0..NCELLS {
        for j in 0..NCELLS {
            v2[[i, j]] = alive(i, j, &v);
        }
    }
    *v = v2;
}

fn init() -> (WindowCanvas, EventPump) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("demo", MAX_X as u32 + 1, MAX_Y as u32 + 1)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = CanvasBuilder::from(window).build().unwrap();

    let event_pump = sdl_context.event_pump().unwrap();

    renderer.set_draw_color(Color::RGB(255, 255, 255));
    renderer.clear();
    renderer.present();

    (renderer, event_pump)
}

pub fn main() {
    let (mut r, mut e) = init();
    let mut v = life_random();

    'running: for _i in 0..4_000 {
        for event in e.poll_iter() {
            if let Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } = event
            {
                break 'running;
            }
        }
        if _i % 1000 == 0 {
            display_frame(&mut r, &v);
        }
        life_next(&mut v);
    }
    // std::thread::sleep(std::time::Duration::from_millis(600000));
}
