/*
 * This file describes command line argument parsing
 * It also defines the default arguments
 */

use itertools::Itertools;
use std::env;
use std::fs::{create_dir, File, OpenOptions};

pub struct Config {
    pub output_dir: Option<String>,
    pub output_file: Option<File>,
    pub lineage_file: Option<File>,
    pub reset_genestates_on_birth: bool,
    pub env_switching: bool,
    pub starting_env: u8,
    pub console_output: bool,
    pub agent_seed: [u8; 32],
    pub field_seed: [u8; 32],
    pub env_seed: [u8; 32],
}

impl Default for Config {
    fn default() -> Self {
        Config {
            output_dir: None,
            output_file: None,
            lineage_file: None,
            reset_genestates_on_birth: false,
            env_switching: true,
            starting_env: 0,
            console_output: false,
            agent_seed: [
                1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
                1, 2, 3, 4,
            ],
            field_seed: [
                1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
                1, 2, 3, 4,
            ],
            env_seed: [
                1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
                1, 2, 3, 4,
            ],
        }
    }
}

pub fn init() -> std::io::Result<Config> {
    let mut cfg = Config::default();
    // let args: Vec<String> = env::args().collect();
    for (arg, val) in env::args().tuple_windows() {
        match (arg.as_str(), val.as_str()) {
            ("-o", dir) => cfg.output_dir = Some(dir.to_string()),
            ("-c", num) => cfg.console_output = num.parse() == Ok(1),
            ("-a", seed) => cfg.agent_seed = [seed.parse().unwrap(); 32],
            ("-e", seed) => cfg.env_seed = [seed.parse().unwrap(); 32],
            ("-f", seed) => cfg.field_seed = [seed.parse().unwrap(); 32],
            ("-switch", num) => cfg.env_switching = num.parse() == Ok(1),
            ("-start_env", num) => cfg.starting_env = num.parse().unwrap(),
            ("-r", num) => cfg.reset_genestates_on_birth = num.parse() == Ok(1),
            (_, _) => {}
        }
    }

    if let Some(dir) = &cfg.output_dir {
        let d = std::path::Path::new(dir);
        create_dir(&d)?;

        let lineage_file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(d.join("lineage"))?;
        let output_file = OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open(d.join("output"))?;

        cfg.output_file = Some(output_file);
        cfg.lineage_file = Some(lineage_file);
    }

    Ok(cfg)
}
